#include <iostream>

#include <p2p_network.h>

class NetworkListener : public INetworkListener
{
    virtual bool NetworkCallback(Network_Message_pb const& msg)
    {
        auto &adv = msg.network_advertise();
        switch (adv.message_case())
        {
            case Network_Advertise_pb::MessageCase::kPeerConnect   :
                std::cout << "Peer " << std::hex << msg.source_id() << " connected to " << msg.dest_id() << std::endl;
                break;

            case Network_Advertise_pb::MessageCase::kPeerDisconnect:
                std::cout << "Peer " << std::hex << msg.source_id() << " disconnected from " << msg.dest_id() << std::endl;
                break;
        }

        return true;
    }
};

using Network = TNetwork<
    uint64_t, // Peer id type     (used to identify peers)
    uint32_t>;// Channel id type  (used for peer id multiplexing)

int main()
{
    // Performs WSAStartup on Windows and nothing on other platforms
    PortableAPI::Socket::InitSocket();

    Network cli1;
    Network cli2;

    NetworkListener listener1;
    NetworkListener listener2;

    // Register our listener object against the NetworkAdvertise message type. (peer connection and disconnection)
    cli1.register_listener(&listener1, 0, Network_Message_pb::MessagesCase::kNetworkAdvertise);
    cli2.register_listener(&listener2, 0, Network_Message_pb::MessagesCase::kNetworkAdvertise);

    // Add to clients a peer_id
    cli1.advertise_peer_id(0xC1);
    cli2.advertise_peer_id(0xC2);

    // Set for a peer_id the network channel we should send network messages to, this allows a single client to have multiple peer ids
    cli1.set_default_channel(0xC1, 0);
    cli2.set_default_channel(0xC2, 0);

    // Start advertising our presence on the network
    // This will advertise all peer_ids and that p2p handshake
    cli1.advertise(true);
    cli2.advertise(true);

    while (1)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        // Default, get all types of messages and dispatch them to listeners
        // Or set the MessagesCase to something else than Network_Message_pb::MessagesCase::MESSAGE_NOT_SET
        // to dispatch only messages matching the MessagesCase
        cli1.ReadNetworkMessages(0/* , Network_Message_pb::MessagesCase::kNetworkAdvertise */);
        cli2.ReadNetworkMessages(0/* , Network_Message_pb::MessagesCase::kNetworkAdvertise */);
    }

}
